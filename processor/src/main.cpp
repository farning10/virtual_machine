#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "Processor.hpp"

#define BUFFER_SIZE 100
#define READ_BUFFER 10

int main()
{
	Processor *test_bed = new Processor();
	int error_code = test_bed->initialize();
	if (error_code)
	{
		printf("initialization error %d\n", error_code);
		return error_code;
	}
	FILE *executable = fopen("exec", "r");
	int place = 0;
	while (executable && !feof(executable) && !ferror(executable))
	{
		fread(test_bed->get_memory() + place, sizeof(int32_t), READ_BUFFER, executable);
		place += READ_BUFFER;
	}

	char buffer[BUFFER_SIZE];
	char prev_buffer[BUFFER_SIZE];
	int running = 1;
	while(running)
	{
		printf("(ndebug): ");
		int i = 0;
		int ch = fgetc(stdin); 
		while(ch != 10 && ch != 13)
		{
			buffer[i++] = ch;
			if (i >= BUFFER_SIZE)
			{
				buffer[BUFFER_SIZE-1] = 0;
				break;
			}
			ch = fgetc(stdin);
		}
		buffer[i] = 0;
		if (buffer[0] == 0)
		{
			printf("%s\n", prev_buffer);
			memcpy(buffer, prev_buffer, BUFFER_SIZE);
		}
		int j = 0;
		while (buffer[j] == ' ') j++;
		int start = j;
		while (buffer[j] != ' ' && buffer[j] != '\0') j++;
		int stop = j;
		char *command = (char*)malloc(stop - start + 1);
		memcpy(command, &buffer[start], stop - start);
		command[stop-start] = 0;
		//again
		while (buffer[j] == ' ') j++;
		start = j;
		while (buffer[j] != ' ' && buffer[j] != '\0') j++;
		stop = j;
		char *argument = (char*)malloc(stop - start + 1);
		memcpy(argument, &buffer[start], stop - start);
		argument[stop-start] = 0;
		// printf("%s, %s\n", command, argument);

		if (!strcmp(command, "q") || !strcmp(command, "quit"))
		{
			running = 0;
		}
		else if (!strcmp(command, "mem")) 
		{
			if (strlen(argument) > 0)
			{
				long mem_position = strtol(argument, 0, 0);
				test_bed->dump_memory(mem_position);
			}
			else
			{
				test_bed->dump_memory(0); //TODO: figure out how to pass dump memory a value from the command line
			}
		}
		else if (!strcmp(command, "n") || !strcmp(command, "next"))
		{
			if (strlen(argument) > 0)
			{
				test_bed->cycle((int)strtol(argument, 0, 0));
			}
			else test_bed->cycle(1);
		}
		else if (!strcmp(command, "r") || !strcmp(command, "register"))
		{
			test_bed->dump_registers();
		}
		else if (!strcmp(command, "b") || !strcmp(command, "break"))
		{
			if(strlen(argument) == 0)
			{
				printf("Break: invalid address\n");
			}
			else
			{
				long address = strtol(argument, 0, 0);
				int id = test_bed->set_breakpoint((uint32_t)address);
				printf("Breakpoint %d set at address 0x%x\n", id, (uint32_t)address);
			}
		}
		else if (!strcmp(command, "c") || !strcmp(command, "continue"))
		{
			test_bed->set_echo(0);
			test_bed->cycle(-1);
			test_bed->set_echo(1);
		}
		else if (!strcmp(command, "list"))
		{
			test_bed->dump_breakpoints();
		}
		else if (!strcmp(command, "delete"))
		{
			if (strlen(argument) > 0)
			{
				int id = (int)strtol(argument, 0, 0);
				int result = test_bed->delete_breakpoint(id);
				if(result) printf("Breakpoint %d deleted\n", id);
				else printf("No Breakpoint %d\n", id);
			}
			else printf("invalid id\n");
		}
		else if (!strcmp(command, "d") || !strcmp(command, "dissassemble"))
		{
			uint32_t start_address = 0;
			if (strlen(argument) > 0)
			{
				start_address = (uint32_t)strtol(argument, 0, 0);
			}
			test_bed->dissassemble(start_address, 100);
		}
		else
		{
			printf("unknown command\n");
		}
		memcpy(prev_buffer, buffer, BUFFER_SIZE);
	}

	return 0;
}
