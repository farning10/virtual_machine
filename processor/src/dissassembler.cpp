#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "dissassembler.hpp"

#define LINE_BUFFER_SIZE 100

const char *opcode_to_str(int opcode, int x_bit, int misc);
const char *condition_to_str(int condition);
const char *register_to_str(int reg);

char *line_dissassemble(uint32_t instruction, uint32_t operand, int *skip)
{
	char buffer[LINE_BUFFER_SIZE];
	int position;

	unsigned int opcode =    (instruction & 0xFF000000) >> 24;
	unsigned int condition = (instruction & 0x00F00000) >> 20;
	unsigned int x_bit =     (instruction & 0x00080000) >> 19;
	unsigned int s_bit =     (instruction & 0x00040000) >> 18;
	unsigned int use_op =    (instruction & 0x00020000) >> 17;
	unsigned int suffix =    (instruction & 0x00010000) >> 16;
	unsigned int misc =       instruction & 0x0000FFFF;

	const char *ins_name = opcode_to_str(opcode, x_bit, misc);
	strncpy(buffer, ins_name, LINE_BUFFER_SIZE);
	int i = 0;
	while (buffer[i] != 0)
	{
		i += 1;
	}
	position = i;

	const char *ins_cond = condition_to_str(condition);
	strncpy(&buffer[position], ins_cond, LINE_BUFFER_SIZE - position);
	if (condition != 4) position += 2;

	if (suffix)
	{
		buffer[position++] = 's';
	}

	if (opcode == 15 || opcode == 16)
	{
		char *result = (char *)malloc((position + 1) * sizeof(char));
		strncpy(result, buffer, position);
		result[position] = 0; //remember the null terminator
		return result;
	}

	if (opcode == 5) /* branch */
	{
		buffer[position++] = ' ';
		buffer[position++] = '#';
		position += snprintf(&buffer[position], LINE_BUFFER_SIZE - position, "0x%x", operand);
		*skip = 1;
	}
	else
	{
		buffer[position++] = ' ';
		int reg = (misc & 0xF000) >> 12;
		const char *ins_arg1 = register_to_str(reg);
		strncpy(&buffer[position], ins_arg1, LINE_BUFFER_SIZE - position);
		if (reg == 10 || reg == 11) position += 3;
		else position += 2;

		buffer[position++] = ',';
		buffer[position++] = ' ';
		if (opcode == 4 || (opcode >= 7 && opcode <= 14) || ((opcode == 1 || opcode == 6) && !s_bit)) /* register */
		{
			int reg_2 = (misc & 0x0F00) >> 8;
			const char *ins_arg2 = register_to_str(reg_2);
			strncpy(&buffer[position], ins_arg2, LINE_BUFFER_SIZE - position);
			if (reg_2 == 10 || reg_2 == 11) position += 3;
			else position += 2;
		}
		else if ((opcode == 1 || opcode == 6) && s_bit) /* constant in misc & 0xFF */
		{
			buffer[position++] = '#';
			int constant = misc & 0x00FF;
			position += snprintf(&buffer[position], LINE_BUFFER_SIZE - position, "0x%x", constant);
		}
		else /* ldr str */
		{
			if (use_op)
			{
				buffer[position++] = '#';
				position += snprintf(&buffer[position], LINE_BUFFER_SIZE - position, "0x%x", operand);
				*skip = 1;
			}
			else
			{
				buffer[position++] = '[';
				int reg_2 = (misc & 0x0F00) >> 8;
				const char *ins_arg2 = register_to_str(reg_2);
				strncpy(&buffer[position], ins_arg2, LINE_BUFFER_SIZE - position);
				if (reg_2 == 10 || reg_2 == 11) position += 3;
				else position += 2;
				buffer[position++] = ']';
			}
		}

		if ((opcode == 2 || opcode == 3) && s_bit)
		{
			buffer[position++] = ',';
			buffer[position++] = ' ';
			buffer[position++] = '#';
			int constant = misc & 0x00FF;
			position += snprintf(&buffer[position], LINE_BUFFER_SIZE - position, "0x%x", constant);
		}
		else if ((opcode >= 7 && opcode <= 10) || opcode == 13 || opcode == 14)
		{
			buffer[position++] = ',';
			buffer[position++] = ' ';
			if (s_bit)
			{
				int constant = misc & 0x00FF;
				position += snprintf(&buffer[position], LINE_BUFFER_SIZE - position, "0x%x", constant);
			}
			else
			{
				int reg_3 = (misc & 0x00F0) >> 4;
				const char *ins_arg3 = register_to_str(reg_3);
				strncpy(&buffer[position], ins_arg3, LINE_BUFFER_SIZE - position);
				if (reg_3 == 10 || reg_3 == 11) position += 3;
				else position += 2;
			}
		}
		else if (opcode == 4)
		{
			buffer[position++] = ',';
			buffer[position++] = ' ';
			if (s_bit)
			{
				buffer[position++] = '#';
				int constant = misc & 0x00FF;
				position += snprintf(&buffer[position], LINE_BUFFER_SIZE - position, "0x%x", constant);
			}
			else
			{
				int reg_3 = (misc & 0x00F0) >> 4;
				const char *ins_arg3 = register_to_str(reg_3);
				strncpy(&buffer[position], ins_arg3, LINE_BUFFER_SIZE - position);
				if (reg_3 == 10 || reg_3 == 11) position += 3;
				else position += 2;
			}
		}
	}
	buffer[position++] = 0;
	char *result = (char *)malloc((position)*sizeof(char));
	strcpy(result, buffer);
	result[position - 1] = 0;
	return result;
}

const char *opcode_to_str(int opcode, int x_bit, int misc)
{
	const char *ins_name;

	switch (opcode)
	{
		case 1: //MOV - move
		{
			ins_name = "mov";
		} break;

		case 2: //LDR - load register
		{
			ins_name = "ldr";
		} break;

		case 3: //STR - store register
		{
			ins_name = "str";
		} break;

		case 4: //ADD - are you a retard?
		{
			if(x_bit) ins_name = "sub";
			else ins_name = "add";
		} break;

		case 5: //BAL - branch always
		{
			if (misc & 0x0001)
			{
				ins_name = "bl";
			}
			else
			{
				ins_name = "b";
			}
		} break;

		case 6: //CMP - compare
		{
			ins_name = "cmp";
		} break;

		case 7: //AND - bitwise and
		{
			ins_name = "and";
		} break;

		case 8: //ORR - bitwise or
		{
			ins_name = "orr";
		} break;

		case 9: //EOR - bitwise exclusive or
		{
			ins_name = "eor";
		} break;

		case 10: //BIC - bit clear (mask)
		{
			ins_name = "bic";
		} break;

		case 11: //NEG - negate (signed)
		{
			ins_name = "neg";
		} break;

		case 12: //NOT - bitwise not
		{
			ins_name = "not";
		} break;

		case 13: //LSL - logical shift left
		{
			ins_name = "lsl";
		} break;

		case 14: //LSR - logical shift right
		{
			ins_name = "lsr";
		} break;

		case 15: //NOP - no operation
		{
			ins_name = "nop";
		} break;

		case 16: //HAL - halt
		{
			ins_name = "hal";
		} break;

		default:
		{
			ins_name = "unknown";
		} break;
	}

	return ins_name;
}


const char *condition_to_str(int condition)
{
	const char *ins_cond;
	switch (condition)
	{
	case 0://eq
		ins_cond = "eq";
		break;

	case 1://ne
		ins_cond = "ne";
		break;

	case 2://vs
		ins_cond = "vs";
		break;

	case 3://vt
		ins_cond = "vt";
		break;

	case 4://al
		ins_cond = "";
		break;

	case 5://nv
		ins_cond = "nv";
		break;

	case 6://hi
		ins_cond = "hi";
		break;

	case 7://ls
		ins_cond = "ls";
		break;

	case 8://pl
		ins_cond = "pl";
		break;

	case 9://mi
		ins_cond = "mi";
		break;

	case 10://cs
		ins_cond = "cs";
		break;

	case 11://cc
		ins_cond = "cc";
		break;

	case 12://ge
		ins_cond = "ge";
		break;

	case 13://lt
		ins_cond = "lt";
		break;

	case 14://gt
		ins_cond = "gt";
		break;

	case 15://le
		ins_cond = "le";
		break;
	}
	return ins_cond;
}

const char *register_to_str(int reg)
{
	const char *ins_arg1;
	switch(reg)
	{
		case 0:
		{
			ins_arg1 = "r0";
		} break;

		case 1:
		{
			ins_arg1 = "r1";
		} break;

		case 2:
		{
			ins_arg1 = "r2";
		} break;

		case 3:
		{
			ins_arg1 = "r3";
		} break;

		case 4:
		{
			ins_arg1 = "r4";
		} break;

		case 5:
		{
			ins_arg1 = "r5";
		} break;

		case 6:
		{
			ins_arg1 = "r6";
		} break;

		case 7:
		{
			ins_arg1 = "r7";
		} break;

		case 8:
		{
			ins_arg1 = "r8";
		} break;

		case 9:
		{
			ins_arg1 = "r9";
		} break;

		case 10:
		{
			ins_arg1 = "r10";
		} break;

		case 11:
		{
			ins_arg1 = "r11";
		} break;

		case 12:
		{
			ins_arg1 = "sr";
		} break;

		case 13:
		{
			ins_arg1 = "sp";
		} break;

		case 14:
		{
			ins_arg1 = "lr";
		} break;

		case 15:
		{
			ins_arg1 = "pc";
		} break;
	}
	return ins_arg1;
}