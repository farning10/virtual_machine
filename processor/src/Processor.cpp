#include "Processor.hpp"
#include "dissassembler.hpp"
#include <stdio.h> //various print statements
#include <stdlib.h> //malloc
#include <assert.h>

#ifdef WIN_32
#include <windows.h>
#endif

void *virtual_allocate(size_t bytes) {
	void *result;
#ifdef WIN_32	
	result = (uint32_t *)VirtualAlloc(NULL, bytes, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
#else
	result = valloc(bytes);
#endif
	return result;
}

int Processor::initialize()
{
	memset(&registers, 0, 16*sizeof(uint32_t));
	size_t bytes = 1006632960 * sizeof(uint32_t);
	memory = (uint32_t *)virtual_allocate(bytes);
	if (memory == NULL)
	{
		printf("Error: Could not allocate memory block\n");
		return 1;
	}
	memset(memory, 0, 256000*sizeof(uint32_t));
	mode = 0;
	terminated = 0;
	echo = 1;
	return 0;
}

void Processor::cycle(int instructions)
{
	if (instructions >= 0)
	{
		int i;
		for(i = 0; i < instructions; i++)
		{
			if(check_break(registers[15])) goto cycle_break;
			fetch_decode_execute(registers[15]);
			if(terminated)
			{
				printf("process terminated\n");
				goto cycle_break;
			}
		}
	}
	else
	{
		while(1)
		{
			if(check_break(registers[15])) goto cycle_break;
			fetch_decode_execute(registers[15]);
			if(terminated)
			{
				printf("process terminated\n");
				goto cycle_break;
			}
		}
	}
	return;
cycle_break:
	printf("Breakpoint reached\n");
}

uint32_t Processor::get_curr_instruction()
{
	return registers[15];
}

uint32_t *Processor::get_registers()
{
	return registers;
}

uint32_t *Processor::get_memory()
{
	return memory;
}

void Processor::dump_registers()
{
	int i;
	for (i = 0; i < 16; i++)
	{
		printf("%d - 0x%x (%d)\n", i, registers[i], registers[i]);
	}
	printf("\n");
}

void Processor::dump_memory(long position)
{
	uint8_t *byte_memory = (uint8_t *)memory;
	int i;
	for (i = position; i < position + 160; i += 16)
	{
		printf( "[0x%08X] ", i );
		print_word(*(uint32_t *)( byte_memory + i ));
		printf("| ");
		print_word(*(uint32_t *)( byte_memory + i + 4 ));
		printf("| ");
		print_word(*(uint32_t *)( byte_memory + i + 8 ));
		printf("| ");
		print_word(*(uint32_t *)( byte_memory + i + 12 ));
		printf( "\n" );
	}
}

void Processor::print_word(uint32_t word)
{
	uint8_t *p_byte = (uint8_t *)&word;
	printf("%02X ", *(p_byte + 0));
	printf("%02X ", *(p_byte + 1));
	printf("%02X ", *(p_byte + 2));
	printf("%02X ", *(p_byte + 3));
}

int Processor::set_breakpoint(uint32_t address) //returns breakpoint ID
{
	Break_item *insert = (Break_item *)malloc(sizeof(Break_item));
	insert->address = address;
	insert->next = 0;
	insert->flag = 0;
	insert->id = 1;
	int id = 1;
	if (!break_list_head)
	{
		break_list_head = insert;
	}
	else
	{
		int max_id = break_list_head->id;
		Break_item *curr = break_list_head;
		while (curr->next)
		{
			curr = curr->next;
			if (curr->id > max_id) max_id = curr->id;
		}
		curr->next = insert;
		curr->next->id = max_id + 1;
		id = max_id + 1;
	}
	return id;
}

int Processor::delete_breakpoint(int id)
{
	if (!break_list_head) return 0;
	Break_item *curr = break_list_head;
	if(curr->id == id)
	{
		break_list_head = curr->next;
		free(curr);
		return 1;
	}
	else 
	{
		while (curr->next)
		{
			if (curr->next->id == id)
			{
				Break_item *temp = curr->next;
				curr->next = curr->next->next;
				free(temp);
				return 1;
			}
			curr = curr->next;
		}
	}
	return 0;
}

void Processor::dump_breakpoints()
{
	if (!break_list_head)
	{
		printf("No breakpoints set\n");
	}
	else
	{
		Break_item *curr = break_list_head;
		while(curr)
		{
			printf("Breakpoint %d at address %x\n", curr->id, curr->address);
			curr = curr->next;
		}
	}
}

void Processor::set_echo(int set)
{
	if(set) echo = 1;
	else echo = 0;
}

void Processor::dissassemble(uint32_t start_address, int lines)
{
	uint32_t curr_instruction = start_address / 4;
	int i;
	for (i = 0; i < lines; i++)
	{
		int skip = 0;
		if (curr_instruction*4 == registers[15]) printf("*");
		printf("0x%04x - %s\n", curr_instruction*4, line_dissassemble(memory[curr_instruction], memory[curr_instruction + 1], &skip));
		curr_instruction++;
		if (skip) curr_instruction++;
	}
}

void Processor::fetch_decode_execute(uint32_t address)
{
	address = address & 0xFFFFFFFF; //this line will need modification - in real mode it should use the entire address space
	uint64_t offset = reinterpret_cast<uint64_t>(memory);
	uint8_t *byte_pointer = reinterpret_cast<uint8_t*>(offset);
	byte_pointer += address;

	Instruction to_pass;
	to_pass.opcode =      (*reinterpret_cast<uint32_t *>(byte_pointer) & 0xFF000000) >> 24; //one byte
	to_pass.condition =   (*reinterpret_cast<uint32_t *>(byte_pointer) & 0x00F00000) >> 20; //four bits
	to_pass.x_bit =       (*reinterpret_cast<uint32_t *>(byte_pointer) & 0x00080000) >> 19; //single bit
	to_pass.s_bit =       (*reinterpret_cast<uint32_t *>(byte_pointer) & 0x00040000) >> 18; //single bit
	to_pass.use_op =      (*reinterpret_cast<uint32_t *>(byte_pointer) & 0x00020000) >> 17; //single bit
	to_pass.suffix =      (*reinterpret_cast<uint32_t *>(byte_pointer) & 0x00010000) >> 16; //single bit
	to_pass.misc =         *reinterpret_cast<uint16_t *>(byte_pointer) & 0x0000FFFF;		//16 bits
	to_pass.operand_1 = *((uint32_t*)byte_pointer + 1);
	to_pass.offset = offset;

	if(echo) printf("[0x%08X] 0x%08X - ", registers[15], *(uint32_t *)byte_pointer);

	if (to_pass.use_op) registers[15] += 8;
	else registers[15] += 4;

	if(conditional(to_pass.condition, registers[12]) && to_pass.opcode != 0) return;

	Flags output;

	switch (to_pass.opcode)
	{
		case 1: //MOV - move
		{
			if(echo) printf("MOV\n");
			output = exec_mov(to_pass);
		} break;

		case 2: //LDR - load register
		{
			if(echo) printf("LDR\n");
			output = exec_ldr(to_pass);
		} break;

		case 3: //STR - store register
		{
			if(echo) printf("STR\n");
			output = exec_str(to_pass);
		} break;

		case 4: //ADD - are you a retard?
		{
			if(echo && !to_pass.x_bit) printf("ADD\n");
			if(echo && to_pass.x_bit) printf("SUB\n");
			output = exec_add_sub(to_pass);
		} break;

		case 5: //BAL - branch always
		{
			if(echo) printf("BRANCH\n");
			output = exec_branch(to_pass);
		} break;

		case 6: //CMP - compare
		{
			if(echo) printf("CMP\n");
			output = exec_cmp(to_pass);
		} break;

		case 7: //AND - bitwise and
		case 8: //ORR - bitwise or
		case 9: //EOR - bitwise exclusive or
		case 10: //BIC - bit clear (mask)
		{
			if(echo)
			{
				switch(to_pass.opcode)
				{
					case 7:
					printf("AND\n");
					break;
					case 8:
					printf("ORR\n");
					break;
					case 9:
					printf("EOR\n");
					break;
					case 10:
					printf("BIC\n");
					break;
				}
			}
			output = exec_bitwise(to_pass);
		} break;

		case 11: //NEG - negate (signed)
		{
			if(echo) printf("NEG\n");
			output = exec_neg(to_pass);
		} break;

		case 12: //NOT - bitwise not
		{
			if(echo) printf("NOT\n");
			output = exec_not(to_pass);
		} break;

		case 13: //LSL - logical shift left
		case 14: //LSR - logical shift right
		{
			if(echo)
			{
				switch(to_pass.opcode)
				{
					case 13:
					printf("LSL\n");
					break;
					case 14:
					printf("LSR\n");
					break;
				}
			}
			output = exec_logic_shift(to_pass);
		} break;

		case 15: //NOP - no operation
		{
			if(echo) printf("NOP\n");
			output = exec_nothing(to_pass);
		} break;

		case 16: //HAL - halt
		{
			if(echo) printf("HAL\n");
			output = exec_halt(to_pass);
		} break;

		default:
		{
			printf("unrecognized instruction \"%d\"\n", to_pass.opcode);
			terminated = 1;
		} break;
	}
	// printf("%d %d %d - %d\n", output.result, output.carry, output.overflow, to_pass.suffix);
	if (to_pass.suffix)
	{
		set_flags(output, &registers[12]);
	}
}

Processor::Flags Processor::exec_mov(const Instruction instruction)
{
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];
	if (instruction.s_bit)
	{
		*destination = instruction.misc & 0xFF;
	}
	else *destination = registers[(instruction.misc & 0x0F00) >> 8];
	Flags flags = {*destination, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_ldr(const Instruction instruction)
{
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];

	uint32_t *source;
	if (instruction.use_op)
	{
		source = (uint32_t *)(instruction.offset + instruction.operand_1);
	}
	else
	{
		int8_t register_offset = 0;
		if (instruction.s_bit)
		{
			register_offset = instruction.misc & 0x00FF;
			// register_offset = (uint32_t)operand_1;
			// printf("bit set (%d)\n", register_offset);
		}
		uint32_t source_address = registers[(instruction.misc & 0x0F00) >> 8];
		// printf("0x%x\n", source_address + register_offset);
		uint64_t real_address = instruction.offset + source_address + register_offset;
		source = (uint32_t *)real_address;
	}

	memcpy(destination, source, 4);
	Flags flags = {*destination, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_str(const Instruction instruction)
{
	uint32_t *source = &registers[(instruction.misc & 0xF000) >> 12];

	uint32_t *destination = NULL;
	if (instruction.use_op)
	{
		destination = (uint32_t *)(instruction.offset + instruction.operand_1);
	}
	else
	{
		int8_t register_offset = 0;
		if (instruction.s_bit)
		{
			register_offset = instruction.misc & 0x00FF;
			// register_offset = (uint32_t)operand_1;
			// printf("bit set (%d)\n", register_offset);
		}
		uint32_t source_address = registers[(instruction.misc & 0x0F00) >> 8];
		// printf("0x%x\n", source_address + register_offset);
		uint64_t real_address = instruction.offset + source_address + register_offset;
		destination = (uint32_t *)real_address;
	}

	memcpy(destination, source, 4);
	Flags flags = {*destination, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_add_sub(const Instruction instruction)
{
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];
	uint32_t addend_1 = registers[(instruction.misc & 0x0F00) >> 8];
	uint32_t addend_2;
	if (instruction.s_bit)
	{
		int8_t temp = instruction.misc & 0x00FF;
		addend_2 = temp;
	}
	else
	{
		addend_2 = registers[(instruction.misc & 0x0F0) >> 4];
	}
	uint32_t sum = 0;
	int carry;
	int overflow;
	if (instruction.x_bit)
	{
		sum = addend_1 - addend_2;
	}
	else
	{
		sum = addend_1 + addend_2;
	}
	asm("movl $0, %0;"
		"movl $0, %1;"
		"JNC add_no_carry;"
		"movl $1, %0;"
		"add_no_carry:"
		"JNO add_no_overflow;"
		"movl $1, %1;"
		"add_no_overflow:"
		: "=r" (carry), "=r" (overflow)
		: /*no input, no clobber*/
		:
	);
	memcpy(destination, &sum, 4);
	Flags flags = {*destination, carry, overflow};
	return flags;
}

Processor::Flags Processor::exec_branch(const Instruction instruction)
{
	if(instruction.misc & 0x0001) registers[14] = registers[15];
	registers[15] = instruction.operand_1;
	Flags flags = {0, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_cmp(const Instruction instruction)
{
	uint32_t arg_1 = registers[(instruction.misc & 0xF000) >> 12];
	uint32_t arg_2;
	if (!instruction.s_bit)
	{
		arg_2 = registers[(instruction.misc & 0x0F00) >> 8];
	}
	else
	{
		arg_2 = instruction.misc & 0x00FF;
	}
	uint32_t sum = arg_1 - arg_2;
	uint32_t flags = 0;
	if (sum == 0) flags = flags | 0x4;
	if ((int32_t)sum < 0) flags = flags | 0x8;
	registers[12] = flags;
	Flags result = {0, 0, 0};
	return result;
}

Processor::Flags Processor::exec_bitwise(const Instruction instruction)
{
	uint32_t arg_1 = registers[(instruction.misc & 0x0F00) >> 8];
	uint32_t arg_2;
	if (!instruction.s_bit)
	{
		arg_2 = registers[(instruction.misc & 0x00F0) >> 4];
	}
	else
	{
		arg_2 = instruction.misc & 0x00FF;
	}
	uint32_t result;
	switch (instruction.opcode)
	{
		case 7: //and
		{
			result = arg_1 & arg_2;
		} break;

		case 8: //and
		{
			result = arg_1 | arg_2;
		} break;

		case 9: //and
		{
			result = arg_1 ^ arg_2;
		} break;

		case 10: //and
		{
			uint32_t mask = 0xFFFFFFFF ^ arg_2;
			result = arg_1 & mask;
		} break;
	}
	int carry = 0;
	int overflow = 0;
	asm("movl $0, %0;"
		"movl $0, %1;"
		"JNC bit_no_carry;"
		"movl $1, %0;"
		"bit_no_carry:"
		"JNO bit_no_overflow;"
		"movl $1, %1;"
		"bit_no_overflow:"
		: "=r"(carry), "=r" (overflow)
		: /*no input, no clobber*/
		:
	);
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];

	*destination = result;
	Flags flags = {result, carry, overflow};
	return flags;
}

Processor::Flags Processor::exec_neg(const Instruction instruction)
{
	uint32_t source = registers[(instruction.misc & 0x0F00) >> 8];
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];
	*destination = ~source + 1;
	//printf("source: %d, destination %d, result %d\n", (instruction.misc & 0x0F00) >> 8, (instruction.misc & 0xF000) >> 12, ~source + 1);
	Flags flags = {*destination, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_not(const Instruction instruction)
{
	uint32_t source = registers[(instruction.misc & 0x0F00) >> 8];
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];
	*destination = ~source;
	Flags flags = {*destination, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_logic_shift(const Instruction instruction)
{
	uint32_t source = registers[(instruction.misc & 0x0F00) >> 8];
	uint32_t *destination = &registers[(instruction.misc & 0xF000) >> 12];
	unsigned int shift_amount;
	if (!instruction.s_bit)
	{
		shift_amount = registers[(instruction.misc & 0x00F0) >> 4];
	}
	else
	{
		shift_amount = instruction.misc & 0x00FF;
	}
	uint32_t result;
	if (instruction.opcode == 13)
	{
		result = source << shift_amount;
	}
	else if (instruction.opcode == 14)
	{
		result = source >> shift_amount;
	}
	int carry = 0;
	int overflow = 0;
	asm("movl $0, %0;"
		"movl $0, %1;"
		"JNC shift_no_carry;"
		"movl $1, %0;"
		"shift_no_carry:"
		"JNO shift_no_overflow;"
		"movl $1, %1;"
		"shift_no_overflow:"
		: "=r"(carry), "=r"(overflow)
		: /*no input, no clobber*/
		:
	);
	*destination = result;
	Flags flags = {*destination, carry, overflow};
	return flags;
}

Processor::Flags Processor::exec_nothing(const Instruction instruction)
{
	Flags flags = {0, 0, 0};
	return flags;
}

Processor::Flags Processor::exec_halt(const Instruction instruction)
{
	terminated = 1;
	Flags flags = {0, 0, 0};
	return flags;
}

void Processor::set_flags(Flags output, uint32_t *flags)
{
	*flags = 0;
	if(output.overflow) /* overflow */
	{
		*flags |= 1;
	}
	if(output.carry) /* carry */
	{
		*flags |= 2;
	}
	if(output.result == 0) /* zero */
	{
		*flags |= 4;
	}
	if(output.result & 0x80000000) /* negative */
	{
		*flags |= 8;
	}
}

bool Processor::conditional(uint8_t condition, uint8_t flags)
{
	switch (condition)
	{
	case 0://eq
		if(flags & 4) return 0;
		break;

	case 1://ne
		if(!(flags & 4)) return 0;
		break;

	case 2://vs
		if(flags & 1) return 0;
		break;

	case 3://vt
		if(!(flags & 1)) return 0;
		break;

	case 4://al
		return 0;
		break;

	case 5://nv
		break;

	case 6://hi
		if((flags & 2) && !(flags & 4)) return 0;
		break;

	case 7://ls
		if(!(flags & 2) && (flags & 4)) return 0;
		break;

	case 8://pl
		if(!(flags & 8)) return 0;
		break;

	case 9://mi
		if(flags & 8) return 0;
		break;

	case 10://cs
		if(flags & 2) return 0;
		break;

	case 11://cc
		if(!(flags & 2)) return 0;
		break;

	case 12://ge
		if((flags & 8) == (flags & 1)) return 0;
		break;

	case 13://lt
		if((flags & 8) != (flags & 1)) return 0;
		break;

	case 14://gt
		if( ( (flags & 8) == (flags & 1) ) && !(flags & 4) ) return 0;
		break;

	case 15://le
		if( ( (flags & 8) != (flags & 1) ) && (flags & 4) ) return 0;
		break;
	}
	return 1;
}

int Processor::check_break(uint32_t address) //return 1 on break
{
	Break_item *curr = break_list_head;
	while (curr)
	{
		if (curr->address == address)
		{
			if(curr->flag)
			{
				curr->flag = 0;
				return 0;
			}
			else
			{
				curr->flag = 1;
				return 1;
			}
		}
		curr = curr->next;
	}
	return 0;
}
