#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <stdint.h>
#include <string.h>

class Processor
{
public:
	int initialize();
	void cycle(int instructions);
	uint32_t get_curr_instruction();
	uint32_t *get_registers();
	uint32_t *get_memory();
	void dump_registers();
	void dump_memory(long position);
	void dump_breakpoints();
	int set_breakpoint(uint32_t address); //returns breakpoint id
	int delete_breakpoint(int id); //returns 1 on success
	void set_echo(int set);
	void dissassemble(uint32_t start_address, int lines);

private:
	struct Instruction
	{
		uint8_t opcode;
		uint8_t condition;
		uint8_t x_bit;
		uint8_t s_bit;
		uint8_t use_op;
		uint8_t suffix;
		uint16_t misc;
		uint32_t operand_1;
		uint64_t offset;
	};

	struct Flags
	{
		uint32_t result;
		int carry;
		int overflow;
	};

	struct Break_item
	{
		Break_item *next;
		uint32_t address;
		int flag;
		int id;
	};

	void print_word(uint32_t word);
	void fetch_decode_execute(uint32_t address);
	Flags inline exec_mov(const Instruction instruction);
	Flags inline exec_ldr(const Instruction instruction);
	Flags inline exec_str(const Instruction instruction);
	Flags inline exec_add_sub(const Instruction instruction);
	Flags inline exec_branch(const Instruction instruction);
	Flags inline exec_cmp(const Instruction instruction);
	Flags inline exec_bitwise(const Instruction instruction);
	Flags inline exec_not(const Instruction instruction);
	Flags inline exec_logic_shift(const Instruction instruction);
	Flags inline exec_nothing(const Instruction instruction);
	Flags inline exec_halt(const Instruction instruction);
	Flags inline exec_neg(const Instruction instruction);
	void set_flags(Flags output, uint32_t *flags);
	bool conditional(uint8_t condition, uint8_t flags);
	int check_break(uint32_t address); //return 1 on break

	uint32_t registers[16]; //  12 = cpsr, 13 = sp, 14 = lr, 15 = pc
	/*
	 *flags: (register 12)
	 *least significant 4 bits
	 *   3      2     1      0
	 *negative zero carry overflow
	 */
	uint32_t *memory;
	Break_item *break_list_head;

	int mode; //0 for real, 1 for protected...
	int terminated; //1 if the process has been terminated
	int echo; //0 for off, 1 for on
};

#endif
