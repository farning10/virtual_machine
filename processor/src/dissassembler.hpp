#ifndef DISSASSEMBLER_H
#define DISSASSEMBLER_H

char *line_dissassemble(uint32_t instruction, uint32_t operand, int *skip);

#endif