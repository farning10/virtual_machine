#include "Assembler.hpp"
#include <math.h> //for ceil()

#define BLOCK_START_SIZE 800 //in words lol

void Assembler::assemble(const char *file, Program_block *final_block)
{
	final_block->start = NULL;
	final_block->length = 0;

	Assembler::g_line_number = 0;
	Assembler::g_no_errors = 1;
	Assembler::g_curr_in_buffer = 0;
	FILE *source_file = fopen(file, "r");
	if(!source_file)
	{
		printf("source file \"%s\" not found\n", file);
		return;
	}

	init_block(&g_inst_block, BLOCK_START_SIZE);
	init_block(&g_data_block, BLOCK_START_SIZE);

	get_tokens(source_file);
	while (g_curr_in_buffer > 0)
	{
		Assembler::cont_assemble();
	}

	resolve_symbols(g_table_head);
	// Symbol *curr_symbol = g_table_head;
	// while (curr_symbol)
	// {
	// 	curr_symbol = curr_symbol->next;
	// }
	fclose(source_file);

	if(g_no_errors)
	{
		unsigned int final_length = g_inst_block.length + g_data_block.length;
		final_block->start = (uint32_t *)malloc(final_length*sizeof(uint32_t));
		final_block->length = final_length;
		memcpy(final_block->start, g_inst_block.start, g_inst_block.length*sizeof(uint32_t));
		memcpy(final_block->start + g_inst_block.length, g_data_block.start, g_data_block.length*sizeof(uint32_t));
	}
}

void Assembler::push_token(Token to_push)
{
	g_buffer[g_curr_in_buffer++] = to_push;
	if (g_curr_in_buffer >= BUFFER_SIZE)
	{
		Assembler::cont_assemble();
	}
}

void Assembler::cont_assemble() //assemble the next line of tokens
{
	Assembler::Token line[30];
	memset(line, 0, sizeof(Assembler::Token)*30);
	int place_in_line = 0;
	Token *curr = g_buffer;
	while ((*curr).type != 11 && (*curr).type != 0 && curr - g_buffer < BUFFER_SIZE)
	{
		memcpy(&line[place_in_line++], curr, sizeof(Assembler::Token));
		curr++;
	}
	Assembler::shift_buffer(g_buffer, place_in_line + 1, BUFFER_SIZE);
	g_line_number++;

	if(line[0].type != 0)
	{
		int bytes;
		bytes = parse(line);
		if (bytes < 0) g_no_errors = 0;
		// if (bytes < 0) printf("Error on line %u.\n", g_line_number);
	}
}

void Assembler::shift_buffer(Token *start, int shift, int size)
{
	int amount = size - shift;
	int i;
	for (i = 0; i < amount; i++)
	{
		*(start + i) = *(start + i + shift);
	}
	memset( start + i, 0, sizeof(Token)*(size - i) );
	g_curr_in_buffer -= shift;
}

int Assembler::mov_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 14)
	{
		to_write |= 1 << 16;
		curr_token++;
	}

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t destination = line[curr_token].value;
	to_write |= destination << 12;
	curr_token += 2; //skip the comma
	if (line[curr_token].type == 8)
	{
		uint8_t source = line[curr_token].value;
		to_write |= source << 8;
	}
	else if (line[curr_token].type == 7)
	{
		uint32_t constant = line[curr_token].value;
		to_write |= 1 << 18;
		to_write |= constant & 0xFF;
	}
	else
	{
		error("Syntax error");
		return -1;
	}

	write_word(&g_inst_block, to_write);
	return 1;
}

int Assembler::ldr_str_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 14)
	{
		to_write |= 1 << 16;
		curr_token++;
	}

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t target_register = line[curr_token].value;
	to_write |= target_register << 12;
	curr_token += 2; // skip the comma

	if (line[curr_token].type == 9)
	{
		uint8_t target_address = line[curr_token].value;
		to_write |= target_address << 8;
		curr_token += 2;
		if (line[curr_token].type == 7)
		{
			int8_t register_offset = static_cast<int8_t>(line[curr_token].value);
			to_write |= 1 << 18;
			to_write |= (uint8_t)register_offset;
			write_word(&g_inst_block, to_write);
			return 1;
		}
		else
		{
			write_word(&g_inst_block, to_write);
			return 1;
		}
	}
	else if (line[curr_token].type == 13)
	{
		pre_add_symbol(
					   reinterpret_cast<char *>(line[curr_token].value),
					   &g_inst_block,
					   g_inst_block.length + 1
					   );

		to_write |= 1 << 17;
		write_word(&g_inst_block, to_write);

		curr_token += 2;

		int32_t register_offset = 0;
		if (line[curr_token].type == 7)
		{
			register_offset = line[curr_token].value;
		}

		write_word(&g_inst_block, register_offset);

		return 2;
	}
	else
	{
		error("Syntax error");
		return -1;
	}
}

int Assembler::add_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	if (line[curr_token].misc)
	{
		to_write |= 1 << 19;
	}
	curr_token++;

	if (line[curr_token].type == 14)
	{
		to_write |= 1 << 16;
		curr_token++;
	}
	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t destination = line[curr_token].value;
	to_write |= destination << 12;
	curr_token += 2; //skipe the comma
	uint8_t addend1 = line[curr_token].value;
	to_write |= addend1 << 8;
	curr_token += 2; //skip the comma
	uint32_t addend2 = line[curr_token].value;

	if (line[curr_token].type == 8)
	{
		to_write |= (addend2 & 0x0000000F) << 4;
	}
	else if (line[curr_token].type == 7)
	{
		int8_t constant = line[curr_token].value & 0xFF;
		to_write |= 1 << 18;
		to_write |= (uint8_t)constant;
	}
	else
	{
		error("Syntax error");
		return -1;
	}

	write_word(&g_inst_block, to_write);
	return 1;
}

int Assembler::branch_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	int misc = line[curr_token].misc;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	if (line[curr_token].type != 13) return -1;

	to_write |= 1 << 17;
	pre_add_symbol(
			   reinterpret_cast<char *>(line[curr_token].value),
			   &g_inst_block,
			   g_inst_block.length + 1
			   );

	if (misc) to_write |= 1;
	else to_write &= 0xFFFFFFFE;

	write_word(&g_inst_block, to_write);
	write_word(&g_inst_block, 0); //reserve space for the address
	return 2;
}

int Assembler::cmp_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t arg_1 = line[curr_token].value;
	to_write |= arg_1 << 12;
	curr_token += 2; //skip the comma
	uint32_t arg_2 = line[curr_token].value;

	if (line[curr_token].type == 8)
	{
		to_write |= (arg_2 & 0x0000000F) << 8;
		write_word(&g_inst_block, to_write);
		return 1;
	}
	else if (line[curr_token].type == 7)
	{
		to_write |= 1 << 18;
		int8_t constant = arg_2 & 0xFF;
		to_write |= (uint8_t)constant;
		write_word(&g_inst_block, to_write);
		return 1;
	}
	else
	{
		error("Syntax error");
		return -1;
	}
}

int Assembler::bitwise_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 14)
	{
		to_write |= 1 << 16;
		curr_token++;
	}

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t destination = line[curr_token].value;
	to_write |= destination << 12;
	curr_token += 2; //skipe the comma
	uint8_t operand_1 = line[curr_token].value;
	to_write |= operand_1 << 8;
	curr_token += 2; //skip the comma
	uint32_t operand_2 = line[curr_token].value;

	if (line[curr_token].type == 8)
	{
		to_write |= (operand_2 & 0x0000000F) << 4;
	}
	else if (line[curr_token].type == 7)
	{
		to_write |= 1 << 18;
		int8_t constant = line[curr_token].value & 0xFF;
		to_write |= (uint8_t)constant;
	}
	else
	{
		error("Syntax error");
		return -1;
	}

	write_word(&g_inst_block, to_write);
	return 1;
}

int Assembler::negate_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 14)
	{
		to_write |= 1 << 16;
		curr_token++;
	}

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t destination = (uint8_t)line[curr_token].value;
	curr_token += 2; //skip the comma
	uint8_t operand = (uint8_t)line[curr_token].value;

	to_write |= destination << 12;
	to_write |= operand << 8;
	write_word(&g_inst_block, to_write);
	return 1;
}

int Assembler::shift_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 14)
	{
		to_write |= 1 << 16;
		curr_token++;
	}

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}

	uint8_t destination = (uint8_t)line[curr_token].value;
	curr_token += 2;
	uint8_t operand = (uint8_t)line[curr_token].value;
	curr_token += 2;
	to_write |= destination << 12;
	to_write |= operand << 8;

	if (line[curr_token].type == 7)
	{
		to_write |= line[curr_token].value & 0xFF;
		to_write |= 1 << 18;
	}
	else if (line[curr_token].type == 8)
	{
		to_write |= (uint8_t)line[curr_token].value << 4;
	}
	else
	{
		return -1;
	}
	write_word(&g_inst_block, to_write);
	return 1;
}

int Assembler::blank_instruction(Token *line)
{
	int curr_token = 0;
	uint32_t to_write = 0;
	to_write |= line[curr_token].value << 24;
	curr_token++;

	if (line[curr_token].type == 6)
	{
		uint8_t condition = line[curr_token].value - 1;
		to_write |= condition << 20;
		curr_token++;
	}
	else
	{
		to_write |= 4 << 20;
	}
	write_word(&g_inst_block, to_write);
	return 1;
}

int Assembler::write_valid_instruction(Token *line)
{
	int result = 0;
	switch ((*line).value)
	{
	case 1:
		//mov
		result = mov_instruction(line);
		break;
	case 2:
	case 3:
		//ldr and str
		result = ldr_str_instruction(line);
		break;
	case 4:
		//add
		result = add_instruction(line);
		break;
	case 5:
		//branch
		result = branch_instruction(line);
		break;
	case 6:
		//compare
		result = cmp_instruction(line);
			break;
	case 7:
	case 8:
	case 9:
	case 10:
		//and, or, exclusive or, bitclear
		result = bitwise_instruction(line);
			break;
	case 11:
	case 12:
		//negate and not
		result = negate_instruction(line);
		break;
	case 13:
	case 14:
		//shifts
		result = shift_instruction(line);
			break;
	case 15:
	case 16:
		//no operation and halt
		result = blank_instruction(line);
			break;
	default:
		error("Syntax error");
		break;
	}
	return result;
}

int Assembler::verify_text(Token *line)
{
	int i;
	for (i = 0; i < NUM_PATTERNS; i++)
	{
		if(patterns[i][0] == (*line).value)
		{
			if (match(line, patterns[i]))
			{
				return 1;
			}
		}
	}
	return 0;
}

int Assembler::space_data(Token *line)
{
	if (line[1].type == 16)
	{
		int reserve = line[1].value;
		reserve = ceil((double)reserve / 4.0);
		int i;
		for (i = 0; i < reserve; i++)
		{
			write_word(&g_data_block, 0);
		}
		return reserve;
	}
	else
	{
		error("Syntax error");
		return -1;
	}
}

int Assembler::byte_data(Token *line)
{
	uint32_t word = 0;
	uint8_t *buffer = (uint8_t *)&word;
	int curr_in_buffer = 0;
	int buffer_end = 4;
	Token *curr_token = &line[1];
	while (curr_token->type > 0)
	{
		if (curr_in_buffer >= buffer_end)
		{
			//buffer = (uint8_t *)realloc(buffer, buffer_end);
			curr_in_buffer = 0;
			write_word(&g_data_block, word);
		}

		if (curr_token->type == 16)
		{
			buffer[curr_in_buffer] = (uint8_t)curr_token->value;
			curr_in_buffer++;
			curr_token++;
		}
		else return -1;

		if (curr_token->type == 12)
		{
			curr_token++;
		}
		else if (curr_token->type != 0)
		{
			error("Syntax error");
			return -1;
		}
	}
	//int result = ceil((double)curr_in_buffer / 4.0);
	//return result;
	return 0;
}

int Assembler::word_data(Token *line)
{
	Token *curr_token = &line[1];
	while (curr_token->type > 0)
	{
		if (curr_token->type == 16)
		{
			//memcpy(&buffer[curr_in_buffer], &(curr_token->value), sizeof(uint32_t));
			write_word(&g_data_block, curr_token->value);
			curr_token++;
		}

		else
		{
			error("Syntax error");
			return -1;
		}

		if (curr_token->type == 12)
		{
			curr_token++;
		}

		else if (curr_token->type != 0)
		{
			error("Syntax error");
			return -1;
		}
	}
	//memcpy(write_loc, buffer, curr_in_buffer*sizeof(uint32_t));
	return 0;
}

int Assembler::write_data(Token *line)
{
	uint32_t to_write;
	switch((*line).value)
	{
		case 1:
		{
			return space_data(line);
		} break;

		case 2:
		{
			return byte_data(line);
		} break;

		case 3:
		{
			return word_data(line);
		} break;

		case 4:
		{
			//stuff goes here
		} break;

		case 5:
		{
			//stuff goes here
		} break;

		default:
		{
			//default
			error("Syntax error");
			return -1;
		} break;
	}
}

int Assembler::process_pseudo_instruction(Token *line)
{
	if (line[0].type != 17 || ( line[0].value != 1 && line[0].value != 2))
	{
		// printf("type: %d, value: %d\n", line[0].type, line[0].value);
		error("Syntax error");
		return 0;
	}
	if (line[1].type == 18 && line[1].value > line[1].misc)
	{
		error("Invalid register range");
		return 0;
	}

	uint8_t register_amount = 0;
	if (line[1].type == 8) /* register */
	{
		uint32_t instruction;
		if (line[0].value == 1)
		{
			instruction = 0x03440D00;
		}
		else
		{
			instruction = 0x02440DFC;
		}
		instruction |= line[1].value << 12;
		write_word(&g_inst_block, instruction);
		register_amount = 1;
	}
	else if (line[1].type == 18) /* register range */
	{
		uint32_t base;
		uint8_t offset;
		uint8_t offset_change;
		unsigned int start;
		unsigned int upper;
		unsigned int lower;
		int increment;
		if (line[0].value == 1)
		{
			base = 0x03440D00;
			offset = 0;
			offset_change = 4;
			start = line[1].value;
			upper = line[1].misc;
			lower = 0;
			increment = 1;
		}
		else
		{
			base = 0x02440D00;
			offset = -4;
			offset_change = -4;
			start = line[1].misc;
			lower = line[1].value;
			upper = line[1].misc;
			increment = -1;
		}

		uint8_t i;
		for (i = start; i <= upper && i >= lower; i += increment)
		{
			uint32_t instruction = base;
			instruction |= i << 12;
			instruction |= offset;
			write_word(&g_inst_block, instruction);
			offset += offset_change;
			register_amount += 1;
		}
	}
	else
	{
		error("Syntax error");
		return 0;
	}

	if (register_amount > 0)
	{
		uint8_t stack_offset = register_amount*4;
		if (line[0].value == 2) stack_offset *= -1;
		uint32_t instruction = 0x0444DD00;
		instruction |= stack_offset;
		write_word(&g_inst_block, instruction);
	}
	return register_amount + 1;
}

int Assembler::parse(Token *line)
{
	if ((*line).type == 4)
	{
		g_mode = (*line).value;
		return 0;
	}
	switch (g_mode)
	{
		case 1:
		{
			if ((*line).type == 5) //instruction
			{
				if (!verify_text(line))
				{
					error("Syntax error");
					return -1;
				}
				else return write_valid_instruction(line); //line is valid, write the line
			}
			else if ((*line).type == 17)
			{
				/*it is a pseudo instruction*/
				return process_pseudo_instruction(line);
			}
			else if ((*line).type == 10) //label
			{
				add_symbol(reinterpret_cast<char *>((*line).value), &g_inst_block, g_inst_block.length);
				return 0;
			}
			else
			{
				error("Syntax error");
				return -1;
			}
		} break;

		case 2:
		{
			if ((*line).type == 10) //label
			{
				add_symbol(reinterpret_cast<char *>((*line).value), &g_data_block, g_data_block.length);
			}
			if ((*line).type == 15)
			{
				int result = write_data(line);
				return result;
			}
		}
	}
	return 0;
}

int Assembler::match(Token *line, const int *pattern)
{
	int j = 1;
	int k = 1;
	while (j < MAX_PATTERN_LENGTH)
	{
		if ( line[k].type == 0 && pattern[j] == 0)
		{
			return 1;
		}
		else if ( line[k].type == abs(pattern[j]) )
		{
			j++;
			k++;
		}
		else if (pattern[j] < 0)
		{
			j++;
		}
		else break;
	}
	return 0;
}

void Assembler::add_symbol(char *name, Write_block *block, unsigned int offset) // this gets called when the assembler
																				// comes accross a label
{
	if (!g_table_head)
	{
		g_table_head = new Symbol();
		g_table_head->name = name;
		g_table_head->block = block;
		g_table_head->offset = offset;
		g_table_head->next = nullptr;
		g_table_head->complete = 1;
		g_table_head->fix_head = nullptr;
		return;
	}
	else
	{
		Symbol *curr = g_table_head;
		while (curr->next)
		{
			if (!strcmp(curr->name, name))
			{
				curr->block = block;
				curr->offset = offset;
				curr->complete = 1;
				//if (curr->fix_head) patch_addresses(curr->fix_head, curr->address);
				return;
			}
			curr = curr->next;
		}
		if (!strcmp(curr->name, name))
		{
			curr->block = block;
			curr->offset = offset;
			curr->complete = 1;
			//if (curr->fix_head) patch_addresses(curr->fix_head, curr->address);
			return;
		}
		curr->next = new Symbol();
		curr->next->name = name;
		curr->next->block = block;
		curr->next->offset = offset;
		curr->next->next = nullptr;
		curr->next->complete = 1;
		curr->next->fix_head = nullptr;
		return;
	}
}

void Assembler::pre_add_symbol(char *name, Write_block *block, unsigned int offset) //this gets called when the assembler
																					//comes across a branch statement
{
	if (!g_table_head)
	{
		g_table_head = new Symbol();
		g_table_head->name = name;
		g_table_head->block = nullptr;
		g_table_head->offset = 0;
		g_table_head->next = nullptr;
		g_table_head->complete = 0;
		hot_fix_insert(&g_table_head->fix_head, block, offset);
	}
	else
	{
		Symbol *curr = g_table_head;
		while (curr->next)
		{
			if (!strcmp(curr->name, name))
			{
				hot_fix_insert(&curr->fix_head, block, offset);
				return;
			}
			curr = curr->next;
		}
		if (!strcmp(curr->name, name))
		{
			hot_fix_insert(&curr->fix_head, block, offset);
			return;
		}
		curr->next = new Symbol();
		curr->next->name = name;
		curr->next->block = nullptr;
		curr->next->offset = 0;
		curr->next->next = nullptr;
		curr->next->complete = 0;
		hot_fix_insert(&curr->next->fix_head, block, offset);
	}
}

void Assembler::hot_fix_insert(Hot_fix **head, Write_block *block, unsigned int offset)
{
	if (!*head)
	{
		*head = new Hot_fix();
		(*head)->next = nullptr;
		(*head)->block = block;
		(*head)->offset = offset;
		return;
	}
	Hot_fix **curr = head;
	while ((*curr)->next)
	{
		curr = &(*curr)->next;
	}
	(*curr)->next = new Hot_fix();
	(*curr)->next->next = nullptr;
	(*curr)->next->block = block;
	(*curr)->next->offset = offset;
}

int Assembler::resolve_symbols(Symbol *head)
{
	Symbol *curr = head;
	while (curr)
	{
		if (curr->complete)
		{
			uint32_t patch;
			if(curr->block == &g_inst_block)
			{
				patch = curr->offset * 4;
			}
			else if (curr->block = &g_data_block)
			{
				patch = (g_inst_block.length + curr->offset)*4;
			}
			else patch = 0;
			patch_addresses(curr->fix_head, patch);
		}
		else
		{
			printf("Undefined label \"%s\".\n", curr->name);
			g_no_errors = 0;
		}
		curr = curr->next;
	}
}

void Assembler::patch_addresses(Hot_fix *head, uint32_t patch)
{
	if (!head) return;
	Hot_fix *curr = head;
	while (curr)
	{
		//*(curr->address) += reinterpret_cast<uint32_t>(patch);
		block_patch_addr(curr->block, curr->offset, patch);
		Hot_fix *temp = curr;
		curr = curr->next;
		delete temp;
	}
}

int Assembler::write_word(Write_block *block, uint32_t to_write)
{
	if (block->length < block->size)
	{
		*(block->start + block->length) = to_write;
		block->length++;
		return 1;
	}
	else
	{
		uint32_t *new_mem = (uint32_t *)malloc(block->size*2*sizeof(uint32_t));
		if (new_mem)
		{
			memcpy(new_mem, block->start, (block->length - 1)*sizeof(uint32_t));
			free(block->start);
			block->size *= 2;
			block->start = new_mem;
			*(block->start + block->length) = to_write;
			block->length++;
			return 1;
		}
		else return 0;
	}
}

int Assembler::init_block(Write_block *block, unsigned int start_size)
{
	block->start = (uint32_t *)malloc(start_size*sizeof(uint32_t));
	if (!block->start) return 0;
	block->size = start_size;
	block->length = 0;
	return 1;
}

int Assembler::block_patch_addr(Write_block *block, unsigned int offset, uint32_t address)
{
	if (!block || offset >= block->length)
	{
		return 0;
	}
	*(block->start + offset) += address;
	return 1;
}

void Assembler::error(const char *message)
{
	printf("%s on line %u\n", message, g_line_number);
}
