#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Assembler.hpp"

int main(int argc, char **argv) {
	Assembler::Program_block assembled;
	if(argc > 1)
	{
		// int option_flags;
		// int i;
		// for (i = 2; i < argc; i++)
		// {
		// 	if () //evidently something goes here
		// }
		Assembler::assemble(argv[1], &assembled);
		if (assembled.length > 0)
		{
			uint32_t *mem_block = assembled.start;
			int i;
			for (i = 0; i < assembled.length; i++)
			{
				printf("%08x\n", mem_block[i]);
			}
			FILE *out = fopen("exec", "w");
			fwrite(mem_block, 4, assembled.length, out);
			FILE *out_readable = fopen("exec.read", "w");
			for (i = 0; i < assembled.length; i++)
			{
				fprintf(out_readable, "%08x\n", mem_block[i]);
			}
			fclose(out);
			fclose(out_readable);
		}
		return 0;
	}
	else return 1;
}
