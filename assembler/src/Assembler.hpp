#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#define BUFFER_SIZE 20
#define NUM_PATTERNS 31
#define MAX_PATTERN_LENGTH 20

#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>

void get_tokens(FILE *);

namespace Assembler {

	struct Token
	{
		int type; //enum
		//  4 - section header
		//  5 - instruction
		//  6 - condition
		//  7 - literal
		//  8 - register
		//  9 - address at register
		// 10 - label
		// 11 - endline
		// 12 - comma
		// 13 - label identifier
		// 14 - set flags (s)
		// 15 - data section type
		// 16 - data literal
		// 17 - pseudo instruction
		// 18 - register range

		uint64_t value; //binary value
		// for instruction:
		// 1 - mov
		// 2 - ldr
		// 3 - str
		// 4 - add, sub
		// 5 - bal
		// 6 - cmp
		// 7 - and
		// 8 - orr
		// 9 - eor
		// 10 - bic
		// 11 - neg
		// 12 - not
		// 13 - lsl
		// 14 - lsr
		// 15 - nop
		// 16 - hal

		// for condition:
		//  1 - eq
		//  2 - ne
		//  3 - vs
		//  4 - vc
		//  5 - al
		//  6 - nv
		//  7 - hi
		//  8 - ls
		//  9 - pl
		// 10 - mi
		// 11 - cs
		// 12 - cc
		// 13 - ge
		// 14 - lt
		// 15 - gt
		// 16 - le

		// for data section types
		// 1 - space
		// 2 - byte
		// 3 - word
		// 4 - ascii
		// 5 - asciiz

		// for pseudo instructions
		// 0 - invalid
		// 1 - push
		// 2 - pop

		int misc;
		//whatever you need
	};

	struct Program_block
	{
		uint32_t *start;
		unsigned int length;
	};

	struct Write_block
	{
		uint32_t *start;
		unsigned int size;
		unsigned int length;
	};

	struct Hot_fix
	{
		Write_block *block;
		unsigned int offset; //number of words
		Hot_fix *next;
	};

	struct Symbol
	{
		Symbol *next;
		char *name;
		Write_block *block;
		unsigned int offset; //number of bytes
		int complete;
		Hot_fix *fix_head;
	};

	static int g_curr_in_buffer;
	static Token g_buffer[BUFFER_SIZE];
	static Symbol *g_table_head;
	static int g_mode; //1 is text, 2 is data
	static Write_block g_data_block;
	static Write_block g_inst_block;
	static unsigned int g_line_number;
	static int g_no_errors;

	void assemble(const char *file, Program_block *final_block);
	void push_token(Token to_push);
	void cont_assemble();
	void shift_buffer(Token *start, int shift, int size); //shifts the buffer left
	void add_symbol(char *name, Write_block *block, unsigned int offset);
	void pre_add_symbol(char *name, Write_block *block, unsigned int offset);
	void hot_fix_insert(Hot_fix **head, Write_block *block, unsigned int offset);
	void patch_addresses(Hot_fix *head, uint32_t patch);
	int resolve_symbols(Symbol *head);
	int parse(Token *line); //takes an array of tokens and write the instruction
	//returns an integer number of words used on success, 0 on no instruction written (blank line or label)
	//returns -1 on failure: invalid line

	int process_pseudo_instruction(Token *line);

	int write_data(Token *line);
	int space_data(Token *line);
	int byte_data(Token *line);
	int word_data(Token *line);
	int ascii_data(Token *line);
	int verify_text(Token *line); //returns a 1 on valid line, 0 on invalid line

	int write_valid_instruction(Token *line);
	int mov_instruction(Token *line);
	int ldr_str_instruction(Token *line);
	int add_instruction(Token *line);
	int branch_instruction(Token *line);
	int cmp_instruction(Token *line);
	int bitwise_instruction(Token *line);
	int negate_instruction(Token *line);
	int shift_instruction(Token *line);
	int blank_instruction(Token *line);

	int match(Token *line, const int *pattern);

	int write_word(Write_block *block, uint32_t to_write);
	int init_block(Write_block *block, unsigned int start_size);
	int block_patch_addr(Write_block *block, unsigned int offset, uint32_t address);

	void error(const char *message);

	const int patterns[NUM_PATTERNS][MAX_PATTERN_LENGTH] = {
		{1, -14, -6, 8, 12, 8},
		{1, -14, -6, 8, 12, 7},
		{2, -14, -6, 8, 12, 9},
		{2, -14, -6, 8, 12, 9, 12, 7},
		{2, -14, -6, 8, 12, 13},
		{2, -14, -6, 8, 12, 13, 12, 7},
		{3, -14, -6, 8, 12, 9},
		{3, -14, -6, 8, 12, 9, 12, 7},
		{3, -14, -6, 8, 12, 13},
		{3, -14, -6, 8, 12, 13, 12, 7},
		{4, -14, -6, 8, 12, 8, 12, 8},
		{4, -14, -6, 8, 12, 8, 12, 7},
		{5, -6, 13},
		{6, -6, 8, 12, 8},
		{6, -6, 8, 12, 7},
		{7, -14, -6, 8, 12, 8, 12, 8},
		{7, -14, -6, 8, 12, 8, 12, 7},
		{8, -14, -6, 8, 12, 8, 12, 8},
		{8, -14, -6, 8, 12, 8, 12, 7},
		{9, -14, -6, 8, 12, 8, 12, 8},
		{9, -14, -6, 8, 12, 8, 12, 7},
		{10, -14, -6, 8, 12, 8, 12, 8},
		{10, -14, -6, 8, 12, 8, 12, 7},
		{11, -14, -6, 8, 12, 8},
		{12, -14, -6, 8, 12, 8},
		{13, -14, -6, 8, 12, 8, 12, 8},
		{13, -14, -6, 8, 12, 8, 12, 7},
		{14, -14, -6, 8, 12, 8, 12, 8},
		{14, -14, -6, 8, 12, 8, 12, 7},
		{15, -6},
		{16, -6}
	};
}

#endif
