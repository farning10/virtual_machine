%{
#include "Assembler.hpp" /* Assembler namespace */
#include <stdlib.h> /* atoi(), strtol() */

int register_number(char *c)
{
	int result;
	if ( tolower(*c) == 'r' )
	{
		result = atoi(c + 1);
	}
	else
	{
		if ( tolower(*c) == 'p' ) result = 15;
		if ( tolower(*c) == 'l' ) result = 14;
		if ( tolower(*c) == 's' )
		{
			if ( tolower(*(c + 1)) == 'p') result = 13;
			else result = 12;
		}
	}
	return result;
}

%}

REGISTER r[0-9]|r1[0-5]|pc|sp|lr|sr
WHITESPACE (\t|\x20)
NUMBER ([0-9]+)
STR_LITERAL (\".*\")
INSTRUCTION (mov|str|ldr|add|sub|b|bl|cmp|and|orr|eor|bic|lsl|lsr|nop|hal|not|neg){WHITESPACE}*(s{WHITESPACE})?
PSEUDO push|pop
%option noyywrap
%option case-insensitive
%option outfile="src/lex.yy.c"

%%

.text|.data												{/*section header*/
															Assembler::Token result;
															result.type = 4;
															if (tolower(yytext[1]) == 't')
															{
																result.value = 1;
																Assembler::push_token(result);
															}
															else if (tolower(yytext[1]) == 'd')
															{
																result.value = 2;
																Assembler::push_token(result);
															}
														}

.ascii|.asciiz|.byte|.word|.space						{ /* data type */
															Assembler::Token result;
															result.type = 15;
															switch(tolower(yytext[1]))
															{
															case 's':
																result.value = 1;
																break;

															case 'b':
																result.value = 2;
																break;

															case 'w':
																result.value = 3;
																break;

															case 'a':
																if (yyleng == 7) result.value = 4;
																else if (yyleng == 8) result.value = 5;
																break;
															}
															Assembler::push_token(result);
														}

[a-zA-Z_][a-zA-Z0-9_]*:									{ /*label*/
															Assembler::Token result;
															result.type = 10;
															result.value = (uint64_t)malloc(yyleng);
															memcpy((void*)result.value, yytext, yyleng - 1);
															*reinterpret_cast<char*>(result.value + yyleng - 1) = 0;
															Assembler::push_token(result);
														}

{REGISTER}												{/* register */
															Assembler::Token result;
															result.type = 8;
															result.value = register_number(yytext);
															Assembler::push_token(result);
														}

\[{REGISTER}\]											{/* address in register */
															Assembler::Token result;
															result.type = 9;
															result.value = register_number(yytext + 1);
															Assembler::push_token(result);
														}

#[+-]?[0-9]*											{/* decimal literal */
															Assembler::Token result;
															result.type = 7;
															result.value = atoi(yytext + 1);
															Assembler::push_token(result);
														}

#[+-]?0x[0-9a-fA-F]*									{/* hexadecimal literal */
															Assembler::Token result;
															result.type = 7;
															result.value = strtol(yytext + 1, 0, 0);
															Assembler::push_token(result);
														}

{INSTRUCTION}											{/* instruction */
															Assembler::Token result;
															result.type = 5;
															switch (tolower(*yytext))
															{

															case 'a':
																if (tolower(*(yytext+1)) == 'd')
																{
																	result.value = 4;
																	result.misc = 0;
																}
																else
																{
																	result.value = 7;
																}
																break;

															case 'b':
																if (tolower(*(yytext+1)) == 'i')
																{
																	result.value = 10;
																}
																else
																{
																	result.value = 5;
																	if (tolower(*(yytext+1))) result.misc = 1;
																	else result.misc = 0;
																}
																break;

															case 'c':
																result.value = 6;
																break;

															case 'e':
																result.value = 9;
																break;

															case 'h':
																result.value = 16;
																break;

															case 'l':
																switch(tolower(*(yytext+1)))
																{
																case 'd':
																	result.value = 2;
																	break;
																case 's':
																	if (tolower(*(yytext+2)) == 'r')
																	{
																		result.value = 14; 
																	}
																	else
																	{
																		result.value = 13;
																	}
																	break;
																}
																break;

															case 'm':
																result.value = 1;
																break;

															case 'n':
																switch(tolower(*(yytext+1)))
																{
																case 'e':
																	result.value = 11;
																	break;
																case 'o':
																	if(tolower(*(yytext+2)) == 't')
																	{
																		result.value = 12;
																	}
																	else
																	{
																		result.value = 15;
																	}
																	break;
																}
																break;

															case 'o':
																result.value = 8;
																break;

															case 's':
																if (tolower(*(yytext + 1)) == 't')
																{
																	result.value = 3;
																}
																else 
																{
																	result.value = 4;
																	result.misc = 1;
																}
																break;

															default:
																result.value = 0;
																break;

															}
															Assembler::push_token(result);
															if (tolower(*(yytext + yyleng - 2)) == 's')
															{
																result.type = 14;
																result.value = 0;
																Assembler::push_token(result);
															}
														}

{PSEUDO}												{/*pseudo-instruction*/
															Assembler::Token result;
															result.type = 17;
															switch ( tolower(*(yytext + 1)) )
															{
															case 'u':
																result.value = 1;
																break;
															case 'o':
																result.value = 2;
																break;
															default:
																result.value = 0;
																break;
															}
															Assembler::push_token(result);
														}

\{{REGISTER}-{REGISTER}\}								{/* range of registers */
															Assembler::Token result;
															result.type = 18;
															result.value = register_number(yytext+1);
															char *c = yytext + 1;
															while (*c != '-' && *c != 0)
															{
																c += 1;
															}
															result.misc = register_number(c + 1);
															Assembler::push_token(result);
														}


eq|ne|vs|vc|al|nv|hi|ls|pl|mi|cs|cc|ge|lt|gt|le			{/* condition code */
															Assembler::Token result;
															result.type = 6;
															switch (tolower(*yytext))
															{
															case 'a':
																result.value = 5;
																break;
															case 'e':
																result.value = 1;
																break;
															case 'n':
																switch (tolower(*(++yytext)))
																{
																case 'e':
																	result.value = 2;
																	break;
																case 'v':
																	result.value = 6;
																	break;
																}
																break;
															case 'v':
																switch(tolower(*(++yytext)))
																{
																case 's':
																	result.value = 3;
																	break;
																case 'c':
																	result.value = 4;
																	break;
																}
																break;
															case 'h':
																result.value = 7;
																break;
															case 'l':
																switch (tolower(*(++yytext)))
																{
																case 's':
																	result.value = 8;
																	break;
																case 't':
																	result.value = 14;
																	break;
																case 'e':
																	result.value = 16;
																	break;
																}
															case 'p':
																result.value = 9;
																break;
															case 'm':
																result.value = 10;
																break;
															case 'c':
																switch (tolower(*(++yytext)))
																{
																case 's':
																	result.value = 11;
																	break;
																case 'c':
																	result.value = 12;
																	break;
																break;
																}
															case 'g':
																switch (tolower(*(++yytext)))
																{
																case 'e':
																	result.value = 13;
																	break;
																}
																case 't':
																	result.value = 15;
																	break;
																break;
															default:
																result.value = 5;
																break;
															}
															Assembler::push_token(result);
														}

@.* 													{/* comment */}

{WHITESPACE}											{/* white space */}

,														{
															Assembler::Token result;
															result.type = 12;
															result.value = 0;
															Assembler::push_token(result);
														}

\n 														{/* newline */
															Assembler::Token result;
															result.type = 11;
															result.value = 0;
															Assembler::push_token(result);
														}

&[a-zA-Z_][a-zA-Z0-9_]*									{/*label identifier*/
															Assembler::Token result;
															result.type = 13;
															result.value = (uint64_t)malloc(yyleng);
															memcpy((void*)result.value, yytext + 1, yyleng - 1);
															*reinterpret_cast<char*>(result.value + yyleng - 1) = 0;
															Assembler::push_token(result);
														}

[+-]?[0-9]+												{/*integer literal*/
															Assembler::Token result;
															result.type = 16;
															result.value = strtoul(yytext, 0, 0);
															Assembler::push_token(result);
														}

[+-]?0[xX][0-9a-fA-F]+									{/*hex integer literal*/
															Assembler::Token result;
															result.type = 16;
															result.value = strtoul(yytext, 0, 0);
															Assembler::push_token(result);
														}

.														{/*default*/
															//Assembler::Token result;
															//result.type = 0;
															//result.value = (uint64_t)malloc(yyleng + 1);
															//strcpy((char *)result.value, yytext);
															//Assembler::push_token(result);
														}

%%

void get_tokens(FILE *input_file)
{
	yyin = input_file;
	yylex();
}